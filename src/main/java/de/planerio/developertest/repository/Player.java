package de.planerio.developertest.repository;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames = {"shirtNumber" , "team_id"})})
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne(optional = false)
    private Team team;

    @Column(columnDefinition = "VARCHAR(3) NOT NULL CHECK (POSITION in  ('GK', 'CB', 'RB', 'LB', 'LWB', 'RWB', " +
            "'CDM', 'CM', 'LM', 'RM', 'CAM', 'ST', 'CF'))")
    private String position;

    @Column(columnDefinition = "INT NOT NULL CHECK (SHIRT_NUMBER >= 1 AND SHIRT_NUMBER <= 99)")
    private int shirtNumber;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(int shirtNumber) {
        this.shirtNumber = shirtNumber;
    }
}
