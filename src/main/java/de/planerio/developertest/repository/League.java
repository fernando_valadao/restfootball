package de.planerio.developertest.repository;

import javax.persistence.*;
import java.util.List;
import javax.validation.constraints.Size;
import org.springframework.validation.annotation.Validated;

@Validated
@Entity
public class League {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false, unique = true)
    private String name;

    @OneToOne(optional = false)
    private Country country;

    @Size(max = 20)
    @OneToMany(targetEntity = Team.class)
    private List<Team> teams;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }
}
