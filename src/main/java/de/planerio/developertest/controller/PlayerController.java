package de.planerio.developertest.controller;

import de.planerio.developertest.repository.Player;
import de.planerio.developertest.repository.PlayerRepository;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/players")
public class PlayerController {
  private final PlayerRepository playerRepo;
  private final int pageSize;

  @Autowired
  public PlayerController(PlayerRepository playerRepo, Integer pageSize) {
    this.playerRepo = playerRepo;
    this.pageSize = pageSize;
  }

  @GetMapping
  public Iterable<Player> getAll(@RequestParam(defaultValue = "0", name = "page") int pageNumber) {
    return playerRepo.findAll(PageRequest.of(pageNumber, pageSize)).getContent();
  }

  @GetMapping("/{id}")
  public Player getById(@PathVariable long id) {
    return playerRepo.findById(id).orElseThrow(EntityNotFoundException::new);
  }

  @PostMapping
  public ResponseEntity<Player> create(@RequestBody Player player) {
    return ResponseEntity.status(HttpStatus.CREATED).body(playerRepo.save(player));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity delete(@PathVariable long id) {
    playerRepo.deleteById(id);
    return ResponseEntity.noContent().build();
  }

  @PutMapping
  public ResponseEntity<Player> update(@RequestBody Player player) {
    if (playerRepo.findById(player.getId()).isEmpty()) {
      throw new EntityNotFoundException();
    }
    return ResponseEntity.ok(playerRepo.save(player));
  }
}
