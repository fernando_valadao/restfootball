package de.planerio.developertest.controller;

import de.planerio.developertest.repository.League;
import de.planerio.developertest.repository.LeagueRepository;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/leagues")
public class LeagueController {
  private final LeagueRepository leagueRepo;
  private final int pageSize;

  @Autowired
  public LeagueController(LeagueRepository leagueRepo, Integer pageSize) {
    this.leagueRepo = leagueRepo;
    this.pageSize = pageSize;
  }

  @GetMapping
  public Iterable<League> getAll(@RequestParam(defaultValue = "0", name = "page") int pageNumber) {
    return leagueRepo.findAll(PageRequest.of(pageNumber, pageSize)).getContent();
  }

  @GetMapping("/{id}")
  public League getById(@PathVariable long id) {
    return leagueRepo.findById(id).orElseThrow(EntityNotFoundException::new);
  }

  @PostMapping
  public ResponseEntity<League> create(@RequestBody League league) {
    return ResponseEntity.status(HttpStatus.CREATED).body(leagueRepo.save(league));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity delete(@PathVariable long id) {
    leagueRepo.deleteById(id);
    return ResponseEntity.noContent().build();
  }

  @PutMapping
  public ResponseEntity<League> update(@RequestBody League league) {
    if (leagueRepo.findById(league.getId()).isEmpty()) {
      throw new EntityNotFoundException();
    }
    return ResponseEntity.ok(leagueRepo.save(league));
  }
}
