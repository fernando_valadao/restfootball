/**
 * Copyright (c) 2020 Google LLC  All rights reserved.
 */

package de.planerio.developertest.controller.advice;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ConstraintViolationAdvice {
  @ResponseBody
  @ExceptionHandler({ConstraintViolationException.class, TransactionSystemException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public String handle(Exception ex) {
    return ex.getMessage();
  }
}
