package de.planerio.developertest.controller;

import de.planerio.developertest.repository.Country;
import de.planerio.developertest.repository.CountryRepository;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/countries")
public class CountryController {

  private final CountryRepository countryRepo;
  private final int pageSize;

  @Autowired
  public CountryController(CountryRepository countryRepo, Integer pageSize) {
    this.countryRepo = countryRepo;
    this.pageSize = pageSize;
  }

  @GetMapping
  public Iterable<Country> getAll(@RequestParam(defaultValue = "0", name = "page") int pageNumber) {
    return countryRepo.findAll(PageRequest.of(pageNumber, pageSize)).getContent();
  }

  @GetMapping("/{id}")
  public Country getById(@PathVariable long id) {
    return countryRepo.findById(id).orElseThrow(EntityNotFoundException::new);
  }

  @PostMapping
  public ResponseEntity<Country> create(@RequestBody Country country) {
    return ResponseEntity.status(HttpStatus.CREATED).body(countryRepo.save(country));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity delete(@PathVariable long id) {
    countryRepo.deleteById(id);
    return ResponseEntity.noContent().build();
  }

  @PutMapping
  public ResponseEntity<Country> update(@RequestBody Country country) {
    if (countryRepo.findById(country.getId()).isEmpty()) {
      throw new EntityNotFoundException();
    }
    return ResponseEntity.ok(countryRepo.save(country));
  }
}
