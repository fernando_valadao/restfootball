package de.planerio.developertest.controller;

import de.planerio.developertest.repository.Team;
import de.planerio.developertest.repository.TeamRepository;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/teams")
public class TeamController {
  private final TeamRepository teamRepo;
  private final int pageSize;

  @Autowired
  public TeamController(TeamRepository teamRepo, Integer pageSize) {
    this.teamRepo = teamRepo;
    this.pageSize = pageSize;
  }

  @GetMapping
  public Iterable<Team> getAll(@RequestParam(defaultValue = "0", name = "page") int pageNumber) {
    return teamRepo.findAll(PageRequest.of(pageNumber, pageSize)).getContent();
  }

  @GetMapping("/{id}")
  public Team getById(@PathVariable long id) {
    return teamRepo.findById(id).orElseThrow(EntityNotFoundException::new);
  }

  @PostMapping
  public ResponseEntity<Team> create(@RequestBody Team team) {
    return ResponseEntity.status(HttpStatus.CREATED).body(teamRepo.save(team));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity delete(@PathVariable long id) {
    teamRepo.deleteById(id);
    return ResponseEntity.noContent().build();
  }

  @PutMapping
  public ResponseEntity<Team> update(@RequestBody Team team) {
    if (teamRepo.findById(team.getId()).isEmpty()) {
      throw new EntityNotFoundException();
    }
    return ResponseEntity.ok(teamRepo.save(team));
  }
}
