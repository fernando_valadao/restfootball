package de.planerio.developertest.controller;

import de.planerio.developertest.repository.Country;
import de.planerio.developertest.repository.CountryRepository;
import de.planerio.developertest.repository.League;
import de.planerio.developertest.repository.LeagueRepository;
import de.planerio.developertest.repository.Player;
import de.planerio.developertest.repository.PlayerRepository;
import de.planerio.developertest.repository.Team;
import de.planerio.developertest.repository.TeamRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
class ControllerApplicationListener implements ApplicationListener<ApplicationReadyEvent> {
  @Autowired
  private PlayerRepository playerRepository;

  @Autowired
  private TeamRepository teamRepository;

  @Autowired
  private LeagueRepository leagueRepository;

  @Autowired
  private CountryRepository countryRepository;

  @Override
  public void onApplicationEvent(ApplicationReadyEvent event) {
    countryRepository.save(provideSpain());
    leagueRepository.save(provideChampionsLeague());
    teamRepository.save(provideAtletico());
    playerRepository.save(provideNeymar());

    countryRepository.save(provideFrance());
    countryRepository.save(provideEngland());
    leagueRepository.save(provideBrasileirao());
    countryRepository.save(provideGermany());

    // store 15 dummy countries to test pagination
    for (int i = 0; i < 15; i++) {
      Country country = new Country();
      country.setLanguage("es");
      country.setName(String.format("Country %d", i));
      countryRepository.save(country);
    }
  }

  @Bean(name = "Spain")
  public Country provideSpain() {
    Country country = new Country();
    country.setId(1);
    country.setLanguage("es");
    country.setName("Spain");
    return country;
  }

  @Bean(name = "ChampionsLeague")
  public League provideChampionsLeague() {
    League league = new League();
    league.setId(2);
    league.setCountry(provideSpain());
    league.setName("Champion's League");
    return league;
  }

  @Bean(name = "Atletico")
  public Team provideAtletico() {
    Team team = new Team();
    team.setId(3);
    team.setLeague(provideChampionsLeague());
    team.setName("Atletico");
    return team;
  }

  @Bean(name = "Neymar")
  public Player provideNeymar() {
    Player player = new Player();
    player.setId(4);
    player.setShirtNumber(10);
    player.setPosition("GK");
    player.setName("Neymar");
    player.setTeam(provideAtletico());
    return player;
  }

  @Bean(name = "France")
  public Country provideFrance() {
    Country country = new Country();
    country.setId(5);
    country.setLanguage("fr");
    country.setName("France");
    return country;
  }

  @Bean(name = "England")
  public Country provideEngland() {
    Country country = new Country();
    country.setId(6);
    country.setLanguage("en");
    country.setName("England");
    return country;
  }

  @Bean(name = "Brasileirao")
  public League provideBrasileirao() {
    League league = new League();
    league.setId(7);
    league.setCountry(provideEngland());
    league.setName("Brasileirao");
    return league;
  }

  @Bean(name = "Germany")
  public Country provideGermany() {
    Country country = new Country();
    country.setId(8);
    country.setLanguage("de");
    country.setName("Germany");
    return country;
  }

  @Bean(name = "ListWith22Teams")
  public List<Team> provideListWith22Teams() {
    List<Team> list = new ArrayList<>();
    for (int i = 0; i < 22; i++) {
      Team team = new Team();
      team.setLeague(provideChampionsLeague());
      team.setName(String.format("Team %d", i));
      list.add(team);
    }
    return list;
  }

  @Bean(name = "ListWith26Players")
  public List<Player> provideListWith26Players() {
    List<Player> list = new ArrayList<>();
    for (int i = 0; i < 26; i++) {
      Player player = new Player();
      player.setName(String.format("Player %d", i));
      player.setPosition("GK");
      player.setShirtNumber(i + 1);
      list.add(player);
    }
    return list;
  }
}
