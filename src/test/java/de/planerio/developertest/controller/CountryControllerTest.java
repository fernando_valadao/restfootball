package de.planerio.developertest.controller;

import static org.assertj.core.api.Assertions.assertThat;

import de.planerio.developertest.repository.Country;
import de.planerio.developertest.repository.CountryRepository;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class CountryControllerTest {
  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private CountryRepository countryRepository;

  @Qualifier("Spain")
  @Autowired
  private Country spain;

  @Qualifier("France")
  @Autowired
  private Country france;

  @LocalServerPort
  private int randomServerPort;

  @Test
  public void getAll_shouldSucceed() throws URISyntaxException {
    final String countriesUrl = String.format("http://localhost:%d/countries", randomServerPort);
    URI uri = new URI(countriesUrl);

    ResponseEntity<List> countries = restTemplate.getForEntity(uri, List.class);

    assertThat(countries.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(countries.getBody()).isNotEmpty();
  }

  @Test
  public void getAll_withPage_shouldSucceed() throws URISyntaxException {
    final String countriesUrl = String.format("http://localhost:%d/countries?page=1", randomServerPort);
    URI uri = new URI(countriesUrl);

    ResponseEntity<List> countries = restTemplate.getForEntity(uri, List.class);

    assertThat(countries.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(countries.getBody()).isNotEmpty();
  }

  @Test
  public void getById_withValidId_shouldSucceed() throws URISyntaxException {
    final String countriesUrl = String.format("http://localhost:%d/countries/%d", randomServerPort, spain.getId());
    URI uri = new URI(countriesUrl);

    ResponseEntity<Country> returnedCountry = restTemplate.getForEntity(uri, Country.class);

    assertThat(returnedCountry.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(returnedCountry.getBody()).isEqualToComparingFieldByField(spain);
  }

  @Test
  public void getById_withInvalidId_shouldReturn404() throws URISyntaxException {
    final String countriesUrl = String.format("http://localhost:%d/countries/%d", randomServerPort, Integer.MAX_VALUE);
    URI uri = new URI(countriesUrl);

    ResponseEntity<Country> returnedCountry = restTemplate.getForEntity(uri, Country.class);

    assertThat(returnedCountry.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  @Test
  public void create_shouldReturnObjectWithId() throws URISyntaxException {
    final String countriesUrl = String.format("http://localhost:%d/countries", randomServerPort);
    URI uri = new URI(countriesUrl);
    Country newCountry = new Country();
    newCountry.setLanguage("it");
    newCountry.setName("Italy");
    HttpEntity<Country> requestEntity = new HttpEntity<>(newCountry);

    ResponseEntity<Country> responseEntity = restTemplate.postForEntity(uri, requestEntity, Country.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    assertThat(responseEntity.getBody().getId()).isGreaterThan(0);
    assertThat(responseEntity.getBody()).isEqualToIgnoringGivenFields(newCountry, "id");
  }

  @Test
  public void create_withInvalidLanguage_shouldReturn400() throws URISyntaxException {
    final String countriesUrl = String.format("http://localhost:%d/countries", randomServerPort);
    URI uri = new URI(countriesUrl);
    Country newCountry = new Country();
    newCountry.setName("Country");
    newCountry.setLanguage("xy");
    HttpEntity<Country> requestEntity = new HttpEntity<>(newCountry);

    ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, requestEntity, String.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void update_shouldSucceed() throws URISyntaxException {
    final String countriesUrl = String.format("http://localhost:%d/countries", randomServerPort);
    URI uri = new URI(countriesUrl);
    spain.setName("Switzerland");
    spain.setLanguage("de");
    HttpEntity<Country> requestEntity = new HttpEntity<>(spain);

    restTemplate.put(uri, requestEntity);

    Optional<Country> persistedCountry = countryRepository.findById(spain.getId());
    assertThat(persistedCountry).isPresent();
    assertThat(persistedCountry.get()).isEqualToComparingFieldByField(spain);
  }

  @Test
  public void delete_shouldSucceed() throws URISyntaxException {
    final String countriesUrl = String.format("http://localhost:%d/countries/%d", randomServerPort, france.getId());
    URI uri = new URI(countriesUrl);

    restTemplate.delete(uri);

    Optional<Country> persistedCountry = countryRepository.findById(france.getId());
    assertThat(persistedCountry).isEmpty();
  }
}
