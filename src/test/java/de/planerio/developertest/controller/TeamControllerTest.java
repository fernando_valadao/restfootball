/**
 * Copyright (c) 2020 Google LLC  All rights reserved.
 */

package de.planerio.developertest.controller;

import static org.assertj.core.api.Assertions.assertThat;

import de.planerio.developertest.repository.League;
import de.planerio.developertest.repository.Player;
import de.planerio.developertest.repository.Team;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TeamControllerTest {
  @Autowired
  private TestRestTemplate restTemplate;

  @LocalServerPort
  private int randomServerPort;

  @Qualifier("ListWith26Players")
  @Autowired
  private List<Player> listwith26Players;

  @Qualifier("ChampionsLeague")
  @Autowired
  private League league;

  @Qualifier("Atletico")
  @Autowired
  private Team team;

  @Test
  public void getAll_shouldSucceed() throws URISyntaxException {
    final String teamsUrl = String.format("http://localhost:%d/teams", randomServerPort);
    URI uri = new URI(teamsUrl);

    ResponseEntity<List> teams = restTemplate.getForEntity(uri, List.class);

    assertThat(teams.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(teams.getBody()).isNotEmpty();
  }

  @Test
  public void getAll_withPage_shouldSucceed() throws URISyntaxException {
    final String teamsUrl = String.format("http://localhost:%d/teams?page=0", randomServerPort);
    URI uri = new URI(teamsUrl);

    ResponseEntity<List> teams = restTemplate.getForEntity(uri, List.class);

    assertThat(teams.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(teams.getBody()).isNotEmpty();
  }


  @Test
  public void getById_withValidId_shouldSucceed() throws URISyntaxException {
    final String teamsUrl = String.format("http://localhost:%d/teams/%d", randomServerPort, team.getId());
    URI uri = new URI(teamsUrl);

    ResponseEntity<Team> returnedTeam = restTemplate.getForEntity(uri, Team.class);

    assertThat(returnedTeam.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(returnedTeam.getBody()).isEqualToIgnoringGivenFields(team, "league", "players");
  }

  @Test
  public void getById_withInvalidId_shouldReturn404() throws URISyntaxException {
    final String teamsUrl = String.format("http://localhost:%d/teams/%d", randomServerPort, Integer.MAX_VALUE);
    URI uri = new URI(teamsUrl);

    ResponseEntity<Team> returnedTeam = restTemplate.getForEntity(uri, Team.class);

    assertThat(returnedTeam.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  @Test
  public void create_shouldReturnObjectWithId() throws URISyntaxException {
    final String teamsUrl = String.format("http://localhost:%d/teams", randomServerPort);
    URI uri = new URI(teamsUrl);
    Team newTeam = new Team();
    newTeam.setLeague(league);
    newTeam.setName("New Team");
    HttpEntity<Team> requestEntity = new HttpEntity<>(newTeam);

    ResponseEntity<Team> responseEntity = restTemplate.postForEntity(uri, requestEntity, Team.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    assertThat(responseEntity.getBody().getId()).isGreaterThan(0);
    assertThat(responseEntity.getBody()).isEqualToIgnoringGivenFields(newTeam, "id", "league");
  }

  @Test
  public void create_withInvalidListOfPlayers_shouldReturn400() throws URISyntaxException {
    final String teamsUrl = String.format("http://localhost:%d/teams", randomServerPort);
    URI uri = new URI(teamsUrl);
    Team newTeam = new Team();
    newTeam.setName("New Team");
    newTeam.setLeague(league);
    newTeam.setPlayers(listwith26Players);
    HttpEntity<Team> requestEntity = new HttpEntity<>(newTeam);

    ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, requestEntity, String.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void update_shouldSucceed() throws URISyntaxException {
    final String teamsUrl = String.format("http://localhost:%d/teams", randomServerPort);
    URI uri = new URI(teamsUrl);
    team.setName("New Name");
    HttpEntity<Team> requestEntity = new HttpEntity<>(team);

    restTemplate.put(uri, requestEntity);
  }

  @Test
  public void delete_shouldSucceed() throws URISyntaxException {
    final String teamsUrl = String.format("http://localhost:%d/teams/%d", randomServerPort, team.getId());
    URI uri = new URI(teamsUrl);

    restTemplate.delete(uri);
  }
}
