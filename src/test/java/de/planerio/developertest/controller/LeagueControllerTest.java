/**
 * Copyright (c) 2020 Google LLC  All rights reserved.
 */

package de.planerio.developertest.controller;

import static org.assertj.core.api.Assertions.assertThat;

import de.planerio.developertest.repository.Country;
import de.planerio.developertest.repository.League;
import de.planerio.developertest.repository.LeagueRepository;
import de.planerio.developertest.repository.Team;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LeagueControllerTest {
  @Autowired
  private TestRestTemplate restTemplate;

  @LocalServerPort
  private int randomServerPort;

  @Autowired
  private LeagueRepository leagueRepository;

  @Qualifier("France")
  @Autowired
  private Country france;

  @Qualifier("Germany")
  @Autowired
  private Country germany;

  @Qualifier("Spain")
  @Autowired
  private Country spain;

  @Qualifier("England")
  @Autowired
  private Country england;

  @Qualifier("ChampionsLeague")
  @Autowired
  private League league;

  @Qualifier("Brasileirao")
  @Autowired
  private League brasil;

  @Qualifier("ListWith22Teams")
  @Autowired
  private List<Team> listWith22Teams;

  @Test
  public void getAll_shouldSucceed() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues", randomServerPort);
    URI uri = new URI(leaguesUrl);

    ResponseEntity<List> leagues = restTemplate.getForEntity(uri, List.class);

    assertThat(leagues.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(leagues.getBody()).isNotEmpty();
  }

  @Test
  public void getAll_withPage_shouldSucceed() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues?page=0", randomServerPort);
    URI uri = new URI(leaguesUrl);

    ResponseEntity<List> leagues = restTemplate.getForEntity(uri, List.class);

    assertThat(leagues.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(leagues.getBody()).isNotEmpty();
  }

  @Test
  public void getById_withValidId_shouldSucceed() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues/%d", randomServerPort, league.getId());
    URI uri = new URI(leaguesUrl);

    ResponseEntity<League> returnedLeague = restTemplate.getForEntity(uri, League.class);

    assertThat(returnedLeague.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(returnedLeague.getBody()).isEqualToIgnoringGivenFields(league, "country", "teams");
  }

  @Test
  public void create_shouldReturnObjectWithId() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues", randomServerPort);
    URI uri = new URI(leaguesUrl);
    League newLeague = new League();
    newLeague.setCountry(germany);
    newLeague.setName("Champion's League 4");
    HttpEntity<League> requestEntity = new HttpEntity<>(newLeague);

    ResponseEntity<League> responseEntity = restTemplate.postForEntity(uri, requestEntity, League.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    assertThat(responseEntity.getBody().getId()).isGreaterThan(0);
    assertThat(responseEntity.getBody()).isEqualToIgnoringGivenFields(newLeague, "id", "country", "teams");
  }

  @Test
  public void create_withInvalidListOfTeams_shouldReturn400() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues", randomServerPort);
    URI uri = new URI(leaguesUrl);
    League newLeague = new League();
    newLeague.setCountry(spain);
    newLeague.setName("Champion's League 4");
    newLeague.setTeams(listWith22Teams);
    HttpEntity<League> requestEntity = new HttpEntity<>(newLeague);

    ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, requestEntity, String.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void create_withTwoLeaguesInSameCountry_shouldReturn400() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues", randomServerPort);
    URI uri = new URI(leaguesUrl);
    League newLeague = new League();
    newLeague.setCountry(spain);
    newLeague.setName("Champion's League 4");
    HttpEntity<League> requestEntity = new HttpEntity<>(newLeague);

    ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, requestEntity, String.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void update_shouldSucceed() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues", randomServerPort);
    URI uri = new URI(leaguesUrl);
    league.setName("New Name");
    HttpEntity<League> requestEntity = new HttpEntity<>(league);

    restTemplate.put(uri, requestEntity);

    Optional<League> returnedLeague = leagueRepository.findById(league.getId());
    assertThat(returnedLeague).isPresent();
    assertThat(returnedLeague.get()).isEqualToIgnoringGivenFields(league, "country", "teams");
  }

  @Test
  public void update_withInvalidId_shouldReturn404() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues", randomServerPort);
    URI uri = new URI(leaguesUrl);
    League newLeague = new League();
    HttpEntity<League> requestEntity = new HttpEntity<>(newLeague);

    restTemplate.put(uri, requestEntity);
  }

  @Test
  public void delete_shouldSucceed() throws URISyntaxException {
    final String leaguesUrl = String.format("http://localhost:%d/leagues/%d", randomServerPort, brasil.getId());
    URI uri = new URI(leaguesUrl);

    restTemplate.delete(uri);
  }
}
