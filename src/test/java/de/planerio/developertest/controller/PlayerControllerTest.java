/**
 * Copyright (c) 2020 Google LLC  All rights reserved.
 */

package de.planerio.developertest.controller;

import static org.assertj.core.api.Assertions.assertThat;

import de.planerio.developertest.repository.Player;
import de.planerio.developertest.repository.Team;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PlayerControllerTest {
  @Autowired
  private TestRestTemplate restTemplate;

  @Qualifier("Neymar")
  @Autowired
  private Player player;

  @Qualifier("Atletico")
  @Autowired
  private Team team;

  @LocalServerPort
  private int randomServerPort;

  @Test
  public void getAll_shouldSucceed() throws URISyntaxException {
    final String playersUrl = String.format("http://localhost:%d/players", randomServerPort);
    URI uri = new URI(playersUrl);

    ResponseEntity<List> players = restTemplate.getForEntity(uri, List.class);

    assertThat(players.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(players.getBody()).isNotEmpty();
  }

  @Test
  public void getAll_withPage_shouldSucceed() throws URISyntaxException {
    final String playersUrl = String.format("http://localhost:%d/players?page=0", randomServerPort);
    URI uri = new URI(playersUrl);

    ResponseEntity<List> players = restTemplate.getForEntity(uri, List.class);

    assertThat(players.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(players.getBody()).isNotEmpty();
  }

  @Test
  public void getById_withValidId_shouldSucceed() throws URISyntaxException {
    final String playersUrl = String.format("http://localhost:%d/players/%d", randomServerPort, player.getId());
    URI uri = new URI(playersUrl);

    ResponseEntity<Player> returnedPlayer = restTemplate.getForEntity(uri, Player.class);

    assertThat(returnedPlayer.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(returnedPlayer.getBody()).isEqualToIgnoringGivenFields(player, "team");
  }

  @Test
  public void getById_withInvalidId_shouldReturn404() throws URISyntaxException {
    final String playersUrl = String.format("http://localhost:%d/players/%d", randomServerPort, Integer.MAX_VALUE);
    URI uri = new URI(playersUrl);

    ResponseEntity<Player> returnedPlayer = restTemplate.getForEntity(uri, Player.class);

    assertThat(returnedPlayer.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  @Test
  public void create_shouldReturnObjectWithId() throws URISyntaxException {
    final String playerUrl = String.format("http://localhost:%d/players", randomServerPort);
    URI uri = new URI(playerUrl);
    Player newPlayer = new Player();
    newPlayer.setShirtNumber(2);
    newPlayer.setPosition("CB");
    newPlayer.setName("Player 2");
    newPlayer.setTeam(team);
    HttpEntity<Player> requestEntity = new HttpEntity<>(newPlayer);

    ResponseEntity<Player> responseEntity = restTemplate.postForEntity(uri, requestEntity, Player.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    assertThat(responseEntity.getBody().getId()).isGreaterThan(0);
    assertThat(responseEntity.getBody()).isEqualToIgnoringGivenFields(newPlayer, "team", "id");
  }

  @Test
  public void create_withInvalidPosition_shouldReturn400() throws URISyntaxException {
    final String playerUrl = String.format("http://localhost:%d/players", randomServerPort);
    URI uri = new URI(playerUrl);
    Player newPlayer = new Player();
    newPlayer.setShirtNumber(4);
    newPlayer.setPosition("XY");
    newPlayer.setName("Player 3");
    newPlayer.setTeam(team);
    HttpEntity<Player> requestEntity = new HttpEntity<>(newPlayer);

    ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, requestEntity, String.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void create_withTwoPlayersHavingSameJerseyNumberInSameTeam_shouldReturn400() throws URISyntaxException {
    final String playerUrl = String.format("http://localhost:%d/players", randomServerPort);
    URI uri = new URI(playerUrl);
    Player newPlayer = new Player();
    newPlayer.setShirtNumber(player.getShirtNumber());
    newPlayer.setPosition("GK");
    newPlayer.setName("Player 3");
    newPlayer.setTeam(team);
    HttpEntity<Player> requestEntity = new HttpEntity<>(newPlayer);

    ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, requestEntity, String.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void create_withInvalidJerseyNumber_shouldReturn400() throws URISyntaxException {
    final String playerUrl = String.format("http://localhost:%d/players", randomServerPort);
    URI uri = new URI(playerUrl);
    Player newPlayer = new Player();
    newPlayer.setShirtNumber(100);
    newPlayer.setPosition("GK");
    newPlayer.setName("Player 4");
    newPlayer.setTeam(team);
    HttpEntity<Player> requestEntity = new HttpEntity<>(newPlayer);

    ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, requestEntity, String.class);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void update_shouldSucceed() throws URISyntaxException {
    final String playersUrl = String.format("http://localhost:%d/players", randomServerPort);
    URI uri = new URI(playersUrl);
    player.setName("Another Name");
    HttpEntity<Player> requestEntity = new HttpEntity<>(player);

    restTemplate.put(uri, requestEntity);
  }

  @Test
  public void delete_shouldSucceed() throws URISyntaxException {
    final String playersUrl = String.format("http://localhost:%d/players/%d", randomServerPort, player.getId());
    URI uri = new URI(playersUrl);

    restTemplate.delete(uri);
  }
}
